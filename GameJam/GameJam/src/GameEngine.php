
<?php

require dirname(__DIR__) . '/vendor/autoload.php';
use Ratchet\MessageComponentInterface;
use Ratchet\ConnectionInterface;
use Ratchet\Http\HttpServerInterface;

/**
 * GameEngine short summary.
 *
 * GameEngine description.
 *
 * @version 1.0
 * @author Graham
 */
class GameEngine implements MessageComponentInterface
{
    protected $clients;
    protected $host;
    protected $players;

    public function __construct(React\EventLoop\LoopInterface $loop)
    {
        $this->clients = new \SplObjectStorage;
        $this->players = new \SplObjectStorage;

        echo "server started\n";
    }

    /**
     * When a new connection is opened it will be passed to this method
     *
     * @param Ratchet\ConnectionInterface $conn The socket/connection that just connected to your application
     */
    function onOpen(Ratchet\ConnectionInterface $conn)
    {
        echo "connected: " . $conn->resourceId . "\n";
    }

    /**
     * This is called before or after a socket is closed (depends on how it's closed).  SendMessage to $conn will not result in an error if it has already been closed.
     *
     * @param Ratchet\ConnectionInterface $conn The socket/connection that is closing/closed
     */
    function onClose(Ratchet\ConnectionInterface $conn)
    {
        echo $conn->resourceId . " has disconnected\n";
    }

    /**
     * If there is an error with one of the sockets, or somewhere in the application where an Exception is thrown,
     * the Exception is sent back down the stack, handled by the Server and bubbled back up the application through this method
     *
     * @param Ratchet\ConnectionInterface $conn
     * @param Exception $e
     */
    function onError(Ratchet\ConnectionInterface $conn, Exception $e)
    {
        echo "An error has occurred: {$e->getMessage()}\n";

        $conn->close();
    }

    /**
     * Triggered when a client sends data through the socket
     *
     * @param Ratchet\ConnectionInterface $from The socket/connection that sent the message to your application
     * @param string $msg The message received
     */
    function onMessage(Ratchet\ConnectionInterface $from, $msg)
    {
        echo "message received\n";
        $message = json_decode($msg);


        //-------------------------Got move from player--------------------
        if (!empty($this->host))
        {
            if ($message->type == "move")
            {
                $move = array(
                        "type" => "move",
                        "id" => $from->resourceId,
                        "move" => $message->move
                    );
                $this->host->send(json_encode($move));
            }
        }


        //------------------------------New Player connected---------------------
        if (!empty($message->clientType))
        {

            if ($message->clientType == "host")
            {
                $this->host = $from;
            }
            elseif ($message->clientType == "player")
            {
                $this->players->attach($from);
                $newplayer = array(
                        'type' => 'newPlayer',
                        'name' => $message->name,
                        'id' => $from->resourceId,
                    );
                if (!empty($this->host))
                {
                    $this->host->send(json_encode($newplayer));
                }

            }
        }



        //------------------------------Start Round---------------------
        if (!empty($this->host) && $from == $this->host)
        {
            if ($message->type == "newRound")
            {
                foreach($this->players as $serverplayer)
                {
                    foreach($message->players as $hostplayer)
                    {
                        if ($serverplayer->resourceId == $hostplayer->id)
                        {
                            $serverplayer->send(json_encode(array("type"=>"newRound", "moves"=>$hostplayer->moves, "roundLength"=>$message->roundLength)));
                        }
                    }
                }
            }
        }

        //-------------------------eliminate a player--------------------

        if (!empty($this->host) && $from == $this->host)
        {
            if ($message->type == "eliminated")
            {
                foreach($this->players as $serverplayer)
                {

                    if ($serverplayer->resourceId == $message->id)
                    {
                        $serverplayer->send(json_encode(array("type"=>"eliminated")));
                    }

                }
            }
        }

        //-------------------------victory--------------------

        if (!empty($this->host) && $from == $this->host)
        {
            if ($message->type == "victory")
            {
                foreach($this->players as $serverplayer)
                {

                    if ($serverplayer->resourceId == $message->id)
                    {
                        $serverplayer->send(json_encode(array("type"=>"victory")));
                    }

                }
            }
        }

    }
}