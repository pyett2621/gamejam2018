<?php
$ip = gethostbyname(gethostname());

?>

<html>
<head>
    <script src="https://code.jquery.com/jquery-3.2.1.js" integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE="
        crossorigin="anonymous"></script>
    <!-- Import Phaser -->
    <script src="./scripts/phaser.js"></script>
    <script src="./scripts/game.js"></script>
    <script>
        var ipaddr = "sumoterry.com";
    </script>
    <link href="./styles/host.css" type="text/css" rel="stylesheet" />
</head>
<div id="background"></div>
<body>
    <div id="lobby">
        <div id="logo">
            <img src="./img/logo.png" />
        </div>
        <div id="ipaddr"></div>
        <div id="players">
            <div class="player" id="player1">
                <img src="./img/sumoTerryYellow.png" />
                <div class="name">Waiting for player</div>
            </div>
            <div class="player" id="player2">
                <img src="./img/sumoTerryBlue.png" />
                <div class="name">Waiting for player</div>
            </div>
            <div class="player" id="player3">
                <img src="./img/sumoTerryGreen.png" />
                <div class="name">Waiting for player</div>
            </div>
            <div class="player" id="player4">
                <img src="./img/sumoTerryRed.png" />
                <div class="name">Waiting for player</div>
            </div>
        </div>
        <div id="start">
            <button id="startgame">START!</button>
        </div>
        <div id="lore">
            <p>Sumo Terry is having a nightmare! He can't do what he wants! He's being FORCED to move in strange ways!<br/>
            Connect to the IP address above and enter your name. When everyone is in, click START!
<br />
            Each turn, you will have three options for actions. Pick one and hope for the best!
<br />
            
                <ul>
                    <li>LEFT, RIGHT, UP, DOWN - move one space in that direction</li>
                    <li>SHOVE - push another sumo directly in front of you back one space</li>
                    <li>CHARGE - run forward and slam into another sumo to send them flying - get a head start to push them farther! But watch out: you might miss!</li>
                </ul>
            </p>
        </div>

    </div>

    <div id="endgame" style="display: none">
        <div id="logo2">
            <img src="./img/logo.png" />
        </div>
        <div id="winner">
            <div id="result">WINNER!</div>
            <img src="./img/sumoTerryYellow.png" />
            <div class="name"></div>
        </div>
        <div id="replay">
            <button id="restart" onclick="location.reload()">PLAY AGAIN!</button>
        </div>
    </div>


    <!--Javascript for WebSockets-->
    <script>
        var lobbyMusic;

        $("#startgame").hide();

        var socket = new WebSocket('ws://sumoterry.com:55555/server');
        gameRunning = false;
        socket.onopen = function (event) {
            console.log('Connected to: ' + event.currentTarget.url);
            socket.send(JSON.stringify({ "clientType": "host" }));
            $("#ipaddr").html("Connect to: " + ipaddr);

        };

        socket.onerror = function (error) {
            console.log('WebSocket Error: ' + error);
        };

        socket.onmessage = function (event) {
            var message = JSON.parse(event.data);
            //console.log(message);

            if (message.type == "move" && gameRunning) {
                processMove(message);
            }
            else if (message.type == "newPlayer" && !gameRunning) {
                if (numPlayers < 4) {
                    numPlayers++;
                    playerIds[message.id] = numPlayers - 1;
                    //console.log(numPlayers);
                    //console.log(".player:nth-of-type(" + numPlayers + ") > img");
                    $(".player:nth-of-type(" + numPlayers + ") .name").html(message.name);
                    playerNames.push(message.name);

                    $(".player:nth-of-type(" + numPlayers + ") > img").css("visibility", "visible");
                    if (numPlayers == 2) {
                        $("#startgame").show();
                    }
                }
            }

        };

        socket.onclose = function (event) {
            console.log('Disconnected from WebSocket.');
        };

        $("#startgame").click(function () {
            lobbyMusic.pause();
            gameRunning = true;
            startGame();
        });

        actionQueue = [];
        numCommands = 0;

        function processMove(message) {

            var playernum = 0;

            var currPlayer;

            for (var i = 0; i < numPlayers; i++) {
                if (players[i].commId == message.id) {
                    currPlayer = players[i];
                }
            }


            numCommands++;

            if (message.move == "up") {
                actionQueue.push({
                    "type": "movement", "player": currPlayer, "function": function () {
                        return movePlayer(currPlayer, 0, -1);
                    }
                });
            }
            else if (message.move == "down") {
                actionQueue.push({
                    "type": "movement", "player": currPlayer, "function": function () {
                        return movePlayer(currPlayer, 0, 1);
                    }
                });
            }
            else if (message.move == "left") {
                actionQueue.push({
                    "type": "movement", "player": currPlayer, "function": function () {
                        return movePlayer(currPlayer, -1, 0);
                    }
                });
            }
            else if (message.move == "right") {
                actionQueue.push({
                    "type": "movement", "player": currPlayer, "function": function () {
                        return movePlayer(currPlayer, 1, 0);
                    }
                });
            }
            else if (message.move == "shove") {
                actionQueue.push({
                    "type": "shove", "player": currPlayer, "function": function () {
                        return shove(currPlayer);
                    }
                })
            }
            else if (message.move == "charge") {
                actionQueue.push({
                    "type": "charge", "player": currPlayer, "function": () => {
                        return charge(currPlayer, 3)
                    }
                })
            }

            if (numCommands == numPlayers) {

                //manage any movement collision


                processShove();
            }
        }

        function processShove() {

            //Process shove queues
            var anyShoves = false;
            actionQueue.forEach(
                item => {
                    if (item.type == "shove") {
                        item.succeeded = item.function();
                        if (item.succeeded) {
                            anyShoves = true;
                        }
                    }
                }
            )

            if (anyShoves) {
                setTimeout(processMovement, 200);

            }
            else {

                processMovement();
            }
        }

        function processMovement() {

            //Process movement queues
            actionQueue.forEach(
                item => {
                    if (item.type == "movement") {
                        item.succeeded = item.function();
                    }
                }
            )

            //Second pass of movement
            actionQueue.forEach(
                item => {
                    if (!item.succeeded && item.type == "movement") {
                        item.function();
                    }
                }
            )

            setTimeout(() => {
                processCharge();
            }, 250);

        }

        function processCharge() {
            var chargeQueue = [];

            for (var i = 0; i < numPlayers; i++) {
                if (actionQueue[i].type == "charge") {
                    chargeQueue.push(actionQueue[i]);
                }
            }
            var i;
            if (chargeQueue.length == 1) {
                chargeQueue[0].function();
            }
            else if (chargeQueue.length > 1) {

                for (i = 0; i < chargeQueue.length; i++) {
                    setTimeout(chargeQueue[i].function, i * 600);
                }
            }


            setTimeout(processWin, i * 600);

        }

        function processWin() {
            actionQueue = [];
            console.log("action queue should be blank:" + actionQueue);
            numCommands = 0;
        }

        $(function () {
            setTimeout(function () {
                lobbyMusic = new Audio('./music/lobby.mp3');
                lobbyMusic.loop = true;
                lobbyMusic.volume = 0.5;
                lobbyMusic.play();
            }, 5);

        });
    </script>

</body>
</html>