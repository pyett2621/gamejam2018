<?php

$ip = gethostbyname(gethostname());

?>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link href="styles/phone_interface.css" rel="stylesheet" />
    <script src="https://code.jquery.com/jquery-3.2.1.js" integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE="
            crossorigin="anonymous"></script>
    <script>
        var ipaddr = "<?= $ip ?>";
    </script>
    <!--Javascript for WebSockets-->
    <script>

        clockValue = 0;
        countdown = 0;
        eliminated = false;

        function connect() {
            socket = new WebSocket('ws://sumoterry.com:55555/server');

            socket.onopen = function (event) {
                $("#launchpage").hide();
                $("#lobby").show();
                
                socket.send(JSON.stringify({ "clientType": "player", "name": $("#name").val() }));
            };

            socket.onerror = function (error) {
                console.log('WebSocket Error: ' + error);
            };

            socket.onmessage = function (event) {
                var message = JSON.parse(event.data);
                if (message.type == "newRound" && !eliminated) {
                    startRound(message.moves, message.roundLength);
                }
                else if (message.type == "eliminated") {
                    eliminated = true;
                    $("#launchpage").hide();
                    $("#lobby").hide();
                    $("#gameinterface").hide();
                    $("#submitted").hide();
                    $("#eliminated").show();
                    $("#victory").hide();
                }
                else if (message.type == "victory") {
                    eliminated = true;
                    $("#launchpage").hide();
                    $("#lobby").hide();
                    $("#gameinterface").hide();
                    $("#submitted").hide();
                    $("#eliminated").hide();
                    $("#victory").show();
                }
            };

            socket.onclose = function (event) {
                console.log('Disconnected from WebSocket.');
            };
        }

        function countDown() {
            $("#clock").html(clockValue -= 1);

            if (clockValue <= 0) {
                //Pick a visible button at random and click it
                //get all visible buttons
                var buttons = $(".move_button:visible");
                //Random number
                var rand = Math.floor(Math.random() * buttons.length)
                buttons[rand].click();                               
            }
        }

        function startRound(moves, roundLength) {
            $("#submitted").hide();
            $("#lobby").hide();
            $("#gameinterface").show();
            clockValue = roundLength;
            $("#clock").html(clockValue);
            countdown = setInterval(countDown, 1000);
            $(".move_button").hide();

            for (i = 0; i < moves.length; i++) {
                $("#" + moves[i]).show();
            }

        }

        function submitAction(message) {
            socket.send(JSON.stringify(message));
            $("#gameinterface").hide();
            $("#submitted").show();
            clearInterval(countdown);


        }

        $(function () {

            $("#submitname").click(function () {
                if ($("#name").val().length >= 1)
                {
                    connect();
                }
                
            });

            var message = {
                "type" : "move",
                "move" : ""
            };

            $("#up").click(function () {
                message.move = "up";
                submitAction(message);
            });

                    $("#down").click(function () {
                        message.move = "down";
                submitAction(message);
            });

                    $("#left").click(function () {
                        message.move = "left";
                submitAction(message);
            });

                    $("#right").click(function () {
                        message.move = "right";
                submitAction(message);
            });

                    $("#shove").click(function () {
                        message.move = "shove";
                submitAction(message);
            });

                    $("#slap").click(function () {
                        message.move = "slap";
                submitAction(message);
            });

                    $("#jump").click(function () {
                        message.move = "jump";
                submitAction(message);
            });

                    $("#duck").click(function () {
                        message.move = "duck";
                submitAction(message);
            });

                    $("#charge").click(function () {
                        message.move = "charge";
                submitAction(message);
            });

                    $("#defend").click(function () {
                        message.move = "defend";
                submitAction(message);
            });

        });

    </script>
</head>
<body>
    <div id="launchpage" style="display: block">
        <div id="logo">
            <img src="./img/logo2.png" />
        </div>
        <div>
            <div id="name_label">
                <label>enter your name</label>

                <input type="text" id="name" maxlength="12" />
            </div>

            <button id="submitname">Join!</button>
        </div>
        <div id="connected"></div>
    </div>

    <div id="lobby" style="display: none">
        <p>WAITING FOR GAME!</p>
    </div>

    <div id="gameinterface" style="display: none">
        <div id="prompt">
            <p id="prompt_text">MAKE YOUR MOVE!</p>
            <p id="clock"></p>
        </div>
        <div id="buttons">
            <button id="up" class="move_button">UP</button>
            <button id="down" class="move_button">DOWN</button>
            <button id="left" class="move_button">LEFT</button>
            <button id="right" class="move_button">RIGHT</button>
            <button id="shove" class="move_button">SHOVE</button>
            <button id="slap" class="move_button">SLAP</button>
            <button id="jump" class="move_button">JUMP</button>
            <button id="duck" class="move_button">DUCK</button>
            <button id="charge" class="move_button">CHARGE</button>
            <button id="defend" class="move_button">DEFEND</button>

        </div>


    </div>
    <div id="submitted" style="display: none">
        <p>MOVE SUBMITTED!</p>
    </div>

    <div id="eliminated" style="display: none">
        <p>ELIMINATED!</p>
        <button class="playAgain" onclick="location.reload()">PLAY AGAIN!</button>
    </div>

    <div id="victory" style="display: none">
        <p>VICTERRY!</p>
        <button class="playAgain" onclick="location.reload()">PLAY AGAIN!</button>
    </div>

</body>
</html>