function startGame() {
    game = new Phaser.Game(704, 704, Phaser.AUTO, '', { preload: preload, create: create, update: update, render: render });
    $("#lobby").hide();
    $("#background").hide();
    var audio = new Audio('./music/gong.mp3');
    audio.play();
}

function preload() {
    //game.load.tilemap('map', 'tilemaps/CircularRingv1.json', null, Phaser.Tilemap.TILED_JSON);
    game.load.tilemap('map', 'tilemaps/TheTrueMap.json', null, Phaser.Tilemap.TILED_JSON);
    game.load.image('MyTileset', 'tilemaps/MyTileset.png');

    game.load.image('YellowSumo', 'img/YellowSumo.png');
    game.load.image('BlueSumo', 'img/BlueSumo.png');
    game.load.image('GreenSumo', 'img/GreenSumo.png');
    game.load.image('RedSumo', 'img/RedSumo.png');

    game.load.audio('music', ['music/music.mp3']);
    game.load.audio('shove', ['sounds/shove.wav']);
    game.load.audio('charge', ['sounds/charge.mp3']);
    game.load.audio('death', ['sounds/death.wav']);

    game.load.image("x","img/x.png");
    game.load.image("whoosh","img/whoosh2.png");
    game.load.image("cloud","img/cloud.png");


}

var emitter;

var playerIds = {};

var players = [];
var playerNames = [];

var numPlayers = 0;
var playerImages = ["YellowSumo", "BlueSumo", "GreenSumo", "RedSumo"];
var frontImages = ["./img/sumoTerryYellow.png", "./img/sumoTerryBlue.png", "./img/sumoTerryGreen.png", "./img/sumoTerryRed.png"];

var tileSize = 64;

function create() {

    music = game.add.audio('music');
    music.loop = true;
    music.play();

    game.physics.startSystem(Phaser.Physics.ARCADE);

    map = game.add.tilemap('map');
    map.addTilesetImage('MyTileset');


    ground = map.createLayer('Ground');
    ground.scale.set(1);
    stage = map.createLayer('Stage');
    stage.scale.set(1);
    stageBorder = map.createLayer('StageBorder');
    stageBorder.scale.set(1);
    ring = map.createLayer("Ring");
    ring.scale.set(1);
    spawn = this.game.add.group();

    spawn.enableBody = true;
    var pspawns = findObjectsByType('SpawnPoint', map, 'Spawn');
    //console.log(pspawns);
    ground.resizeWorld();

    var style = { font: "16px osaka", fill: "#FFFFFF", align: "center", backgroundColor: "#000000" };

    for (var i = 0; i < numPlayers; i++) {

        players[i] = game.add.sprite(pspawns[i].x + tileSize / 2, pspawns[i].y + tileSize / 2, playerImages[i]);
        game.physics.enable(players[i], Phaser.Physics.ARCADE);
        players[i].body.collideWorldBounds = true;
        players[i].anchor.setTo(0.5, 0.5);
        players[i].body.setSize(200, 200,55,55);
        players[i].scale.setTo(0.2, 0.2);

        switch (i) {
            case 0:
                players[i].angle = 90;
                break;
            case 1:
                players[i].angle = -90;
                break;
            case 2:
                players[i].angle = 180;
                break;
            case 3:
                players[i].angle = 0;
                break;
        }


        players[i].name = playerNames[i];
        players[i].isAlive = true;
        players[i].frontImage = frontImages[i];
        //store the player's current coordinates
        players[i].col = ((pspawns[i].x) / tileSize);
        players[i].row = ((pspawns[i].y) / tileSize);


        var text = game.add.text(0, 0, " " + players[i].name + " ", style);
        text.anchor.set(0.5,0);
        players[i].text = text;

        players[i].clouds = game.add.emitter(0,0,40);
        players[i].clouds.makeParticles('cloud');


        //console.log(pspawns[i].x + ":" + pspawns[i].y);
    }
    var increment = 0;
    for (id in playerIds) {
        players[increment].commId = id;
        increment++;
    }

    emitter = game.add.emitter(0, 0, 40);
    emitter.makeParticles('whoosh');


    setTimeout(newRound, 500);
    gameClock = setInterval(newRound, 6500);

}

function update() {
    for (var i = 0; i < numPlayers; i++) {
        players[i].text.x = players[i].x;
        players[i].text.y = players[i].y + tileSize / 2;

        players[i].clouds.x = players[i].x;
        players[i].clouds.y = players[i].y;
    }
}

//helper method of create ladders, might be expanded
function findObjectsByType(type, map, layer) {
    //find objects within object layer?
    var result = new Array();

    map.objects[layer].forEach(function (element) {
        if (element.type === type) {
            //Phaser uses top left, Tiled bottom left so we have to adjust
            //also keep in mind that the cup images are a bit smaller than the tile which is 16x16
            //so they might not be placed in the exact position as in Tiled

            result.push(element);
        }
    }, this);
    return result;
}

function render() {
    //game.debug.bodyInfo(player, 32, 32);
    //players.forEach(element => {
    //   game.debug.body(element);
    //});
    //game.debug.body(player);
}

//Set up a new round and request moves from players
function newRound() {
    checkForDeath();
    checkForWin();

    var playersArray = [];


    for (var i = 0; i < numPlayers; i++) {

        var tempMoves = ["up", "down", "left", "right", "shove", "charge"];

        for (var k = 0; k < 3; k++) {
            tempMoves.splice(Math.floor(Math.random() * tempMoves.length), 1);
        }

        console.log(tempMoves);

        playersArray.push({ "id": players[i].commId, "moves": tempMoves });
    }

    var message = {
        "type": "newRound",
        "players": playersArray,
        "roundLength": "5"
    }

    socket.send(JSON.stringify(message));

}

//gets you the center point of the tile based on the row and column
function getTileCoords(col, row) {
    return { x: (col * tileSize) + tileSize / 2, y: (row * tileSize) + tileSize / 2 };
}

function movePlayer(actionPlayer, x, y) {
    ////console.log(actionPlayer.key + " is facing " + actionPlayer.angle);
    var angle;
    /*
    if(x > 0)
    {
        if (actionPlayer.angle == -180)
        {
            actionPlayer.angle = 180;
            //console.log(actionPlayer.key + " is now facing " + actionPlayer.angle + " before rotating");
        }
        angle = 90;
    }
    else if (x < 0)
    {
        if (actionPlayer.angle == 180)
        {
            actionPlayer.angle = -180;
        }
        angle = -90;
    }
    else if( y> 0)
    {
        angle = actionPlayer.angle;

        if (actionPlayer.angle == -90)
        {
            angle = -180;
        }
        else if (actionPlayer.angle == 90)
        {
            angle = 180;
        }
        else if (actionPlayer.angle == 0)
        {
            angle = 180;
        }
        
        
    }
    else if (y < 0)
    {
        angle = 0;
    }
    */
    if (x > 0) {
        angle = 90;
    }
    else if (x < 0) {
        angle = -90;
    }
    else if (y > 0) {
        angle = 180;
    }
    else if (y < 0) {
        angle = 0;
    }
    actionPlayer.angle = angle;
    //var newAngle = angle - game.math.getShortestAngle(angle, actionPlayer.angle);

    var spaceFree = true;

    for (i = 0; i < numPlayers; i++) {
        if (players[i].row == actionPlayer.row + y && players[i].col == actionPlayer.col + x) {
            spaceFree = false;
        }
    }

    if (spaceFree) {
        actionPlayer.col += x;
        actionPlayer.row += y;

        newPos = getTileCoords(actionPlayer.col, actionPlayer.row);

        //var rotate = game.add.tween(actionPlayer).to({angle: angle},100, Phaser.Easing.Default, true);

        var move = game.add.tween(actionPlayer).to({ x: newPos.x, y: newPos.y }, 200, Phaser.Easing.Default, true);

    }
    checkForDeath();
    return spaceFree;
}


function shove(actionPlayer) {



    //figure out what space is being shoved
    shovesound = game.add.audio('shove');
    shovesound.play();

    var shoveSpace = {};

    var row = 0;
    var col = 0;

    if (actionPlayer.angle == 0) {
        shoveSpace.row = actionPlayer.row - 1;
        shoveSpace.col = actionPlayer.col;
        row = -1;
        emitter.setXSpeed(-150,150);
        emitter.setYSpeed(-400,-450);
    }
    else if (actionPlayer.angle == 90) {
        shoveSpace.row = actionPlayer.row;
        shoveSpace.col = actionPlayer.col + 1;
        col = 1;
        emitter.setXSpeed(400,450);
        emitter.setYSpeed(-150,150);
    }
    else if (actionPlayer.angle == 180 || actionPlayer.angle == -180) {
        shoveSpace.row = actionPlayer.row + 1;
        shoveSpace.col = actionPlayer.col;
        row = 1
        emitter.setXSpeed(-150,150);
        emitter.setYSpeed(400,450);
    }
    else if (actionPlayer.angle == -90) {
        shoveSpace.row = actionPlayer.row;
        shoveSpace.col = actionPlayer.col - 1;
        col = -1;
        emitter.setXSpeed(-400,-450);
        emitter.setYSpeed(-150,150);
    }

    emitter.x = actionPlayer.x;
    emitter.y = actionPlayer.y;
    emitter.start(true, 150, null, 10);

    //move anyone on that space the correct direction
    var shoveSucceeded = false;


    for (i = 0; i < numPlayers; i++) {
        if (players[i].row == shoveSpace.row && players[i].col == shoveSpace.col) {

            var spaceFree = true;

            for (j = 0; j < numPlayers; j++) {
                if (players[j].row == players[i].row + row && players[j].col == players[i].col + col) {
                    spaceFree = false;
                }
            }

            if (spaceFree) {
                players[i].row += row;
                players[i].col += col;
                shoveSucceeded = true;
            }

            if (shoveSucceeded) {
                var newPos = getTileCoords(players[i].col, players[i].row);
                var move = game.add.tween(players[i]).to({ x: newPos.x, y: newPos.y }, 100, Phaser.Easing.Default, true);
            }
        }
    }
    checkForDeath();
    return shoveSucceeded;
}

function checkForDeath() {
    var tempnumplayers = numPlayers;
    for (i = 0; i < tempnumplayers; i++) {
        game.physics.arcade.overlap(players[i], ground, undefined,
            function (player, tile) {
                if (tile.index != -1 && player.isAlive) {
                    death = game.add.audio('death');
                    death.play();

                    player.isAlive = false;

                    numPlayers--;

                    //console.log("This player is dead: " + player.commId);
                    socket.send(JSON.stringify({ "type": "eliminated", "id": player.commId }));

                    var x = game.add.sprite(player.x, player.y, "x");
                    x.anchor.setTo(0.5,0.5);
                    x.scale.setTo(0.08,0.08);

                    //need to still alter player image\
                    //Clear actuon queue
                    for (var k = 0; k < actionQueue.length; k++) {
                        if (actionQueue[k].player == player) {
                            actionQueue.splice(k, 1);
                            break;
                        }
                    }
                }
            });
    };

    var tempplayers = [];
    for (i = 0; i < tempnumplayers; i++) {
        if (players[i].isAlive) {
            tempplayers.push(players[i]);
        }
    }

    players = tempplayers;
    //console.log(players);
    
}

function checkForWin() {
    //console.log("checking for win. NumPlayers " + numPlayers);
    if (numPlayers == 1) {
        game.destroy();
        $("#background").show();
        $("#endgame").show();
        $("#result").html("WINNER!");
        $("#winner img").show();
        $("#winner .name").show();
        var winner = "";
        var winnerImg = "";

        winner = players[0].name;
        winnerImg = players[0].frontImage;

        $("#winner img").attr("src", winnerImg);

        $("#winner .name").html(winner);
        clearInterval(gameClock);

        var victoryMusic = new Audio("./music/victory.mp3");
        victoryMusic.loop = true;
        victoryMusic.play();

        socket.send(JSON.stringify({ "type": "victory", "id": players[0].commId }));
    }
    else if (numPlayers == 0) {

        game.destroy();
        $("#background").show();
        $("#endgame").show();
        $("#result").html("DRAW!");
        $("#winner img").hide();
        $("#winner .name").hide();
        clearInterval(gameClock);

        var victoryMusic = new Audio("./music/victory.mp3");
        victoryMusic.loop = true;
        victoryMusic.play();
    }
}

function charge(actionPlayer, numSpaces) {
    chargesound = game.add.audio('charge');
    chargesound.play();

    actionPlayer.clouds.start(false, 150, 25, 6);

    var row = 0;
    var col = 0;

    if (actionPlayer.angle == 0) {
        row = -1;
    }
    else if (actionPlayer.angle == 90) {
        col = 1;
    }
    else if (actionPlayer.angle == 180 || actionPlayer.angle == -180) {
        row = 1;
    }
    else if (actionPlayer.angle == -90) {
        col = -1;
    }

    var newRow;
    var newCol;

    var hit = false;

    //check path
    for (var i = 2; i <= numSpaces; i++) {
        for (var j = 0; j < numPlayers; j++) {
            if (players[j].col == actionPlayer.col + i * col && players[j].row == actionPlayer.row + i * row) {
                //player is there!

                //set where to takeover
                newRow = actionPlayer.row + i * row;
                newCol = actionPlayer.col + i * col;
                var hit = true;

                //new position for the player
                // var otherRow = players[j].row + i * row;
                // var otherCol = players[j].col + i * col;
                var otherRow = players[j].row;
                var otherCol = players[j].col;

                //is there a player behind them?
                var movedAny = false;
                var k;
                for(k = 1; k<= i;k++)
                {
                    var playerBehind = false;
                    for(var l = 0; l < numPlayers; l++)
                    {
                        if(players[l].col == otherCol + col && players[l].row == otherRow + row)
                        {
                            playerBehind = true;
                            break;
                        }
                    }
                    if(playerBehind)
                    {
                        break;
                    }
                    else
                    {
                        otherRow += row;
                        otherCol += col;
                        var movedAny = true;
                    }
                }

                if(!movedAny)
                {
                    newRow -= row;
                    newCol -= col;
                }

                var numPushed = k;
                

                var chargerCoords = getTileCoords(newCol, newRow);
                var otherCoords = getTileCoords(otherCol, otherRow);

                var time = i * 100;


                //console.log("Player is moving " + (newRow - actionPlayer.row) + " rows");
                //console.log("Player is moving " + (newCol - actionPlayer.col) + " cols");

                actionPlayer.row = newRow;
                actionPlayer.col = newCol;

                var chargeTween = game.add.tween(actionPlayer).to({ x: chargerCoords.x, y: chargerCoords.y }, time, Phaser.Easing.Default, true);

                players[j].row = otherRow;
                players[j].col = otherCol;
                setTimeout(function () {
                    var knockbackTween = game.add.tween(players[j]).to({ x: otherCoords.x, y: otherCoords.y }, numPushed * 100, Phaser.Easing.Default, true);
                }, time - 100);

                break;
            }
        }

        //break out of the second loop if hit
        if (hit) {
            break;
        }

        
    }

    var tooClose = false;
    for (var i = 0; i < numPlayers; i++) {
        //if this player is in the 1st space we'll travel to
        if (players[i].col == actionPlayer.col + col && players[i].row == actionPlayer.row + row) {
            tooClose = true;
            break;
        }
    }

    //Haven't hit anybody, and there isn't someone who's too close
    if (!hit && !tooClose) {
        //the place where you would normally stop, we know a player isn't there
        var newRow = actionPlayer.row + row * (numSpaces);
        var newCol = actionPlayer.col + col * (numSpaces);

        for (var i = 1; i <= 2; i++) {
            //console.log("Player 1 coords: " + newRow + ":" + newCol);
            //console.log("Player 2 coords: " + players[1].row + ":" + players[1].col);
            var playerThere = false;
            for (var j = 0; j < numPlayers; j++) {
                //console.log("Would now move to " + (newRow + row) + ":" + (newCol + col));
                //If there isn't a player there
                //console.log("The player we're checking is at : " + players[j].col + ":" + players[j].row);
                if (players[j].col == (newCol + col) && players[j].row == (newRow + row)) {
                    playerThere = true;
                    break;
                }
            }
            if (playerThere) {
                break;
            }
            else {
                //move 1 more space in the desired direction
                newRow += row;
                newCol += col;
            }
        }

        //console.log("Player is moving " + (newRow - actionPlayer.row) + " rows");
        //console.log("Player is moving " + (newCol - actionPlayer.col) + " cols");

        actionPlayer.row = newRow;
        actionPlayer.col = newCol;
        //Move 2 more spaces away (because you had to slow down)
        var chargerCoords = getTileCoords(newCol, newRow);
        game.add.tween(actionPlayer).to({ x: chargerCoords.x, y: chargerCoords.y }, 300, Phaser.Easing.Default, true);
    }
    checkForDeath();

    return hit;
}