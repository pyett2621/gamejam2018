# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

GameJam 2018 Submission

Sumo Terry's Nightmare

### How do I get set up? ###

Edit ServePage.bat file to have the current systems ip address. 
	Additional instructions included in file.
	
Install PHP on machine.

Run the following command in a command prompt navigated to the main project directory:
	
	php composer.phar install

Run both ServePage.bat and GameServer.bat files.

Navigate to <local ip address>/Host.php (where <local ip address> is the ip hat should be set in the ServePage.bat file)

Connect mobile devices to the same network as the host computer.

On mobile devices navigate to the local ip address of the host computer in a web browser.


### Who do I talk to? ###

For additional help or instructions, contact graham.pyett@gmail.com